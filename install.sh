#! /bin/bash

# install mjpg streamer
sudo apt-get install -y cmake libjpeg8-dev

git clone https://github.com/jacksonliam/mjpg-streamer.git

cd mjpg-streamer/mjpg-streamer-experimental
make
sudo make install
