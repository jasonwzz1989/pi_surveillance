#! /bin/bash

# run for 12 hours
raspistill -tl 3000 -t 43200999 -vf -w 480 -h 360 -o ~/mjpg/test.jpg -n -q 10 &

backup_dir="/home/pi/backup/"$(date '+%Y%m%d')
mkdir $backup_dir
i=0
while [ $i -lt 14400 ]
do
i=$(($i+1))
filename=`date '+%Y%m%d_%H%M%S'`.jpg
cp /home/pi/mjpg/test.jpg $backup_dir/$filename
sleep 3
done
